<?php
/**
 * Created by PhpStorm.
 * User: Mahbubul Alam
 * Date: 10/8/2017
 * Time: 6:13 AM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: Mahbubul Alam
 * Date: 10/7/2017
 * Time: 10:58 PM
 */
?>
<?php
include_once '../includes/header.php';
include_once '../../../vendor/autoload.php';
error_reporting(0);

$placeData = new App\front\Place();
$data = $placeData->singlePlace($_GET['id']);
?>


    <div class="container">

            <div class="visit-top">
                <h3 style="margin: 20px 0;">Tourist places in Bangladesh</h3>
            </div>
            <div class="text-center">
                <div class="row">
                    <div class="col-md-12" align="center">
                        <img width="600px" src="view/admin/uploads/<?php echo  $data['image'];?>" class="img-responsive" alt="no image" />
                    </div>

                    <div class="col-md-12">


                        <h2 style="margin: 15px 0";><?php echo  $data['title'];?></h2>
                        <p style="font-weight: normal;font-size: 12px;color: #333;"><?php echo  $data['content'];?></p>
                     <span class="#">Category: <?php echo  $data['cat'];?></span>


                        </div>

                    </div>
                </div>




            </div>

<!--visit places end here-->

<?php
include_once 'includes/footer.php';

?>

