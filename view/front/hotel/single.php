<?php
/**
 * Created by PhpStorm.
 * User: Mahbubul Alam
 * Date: 10/7/2017
 * Time: 10:58 PM
 */
?>
<?php
include_once '../includes/header.php';
include_once '../../../vendor/autoload.php';
error_reporting(0);

$hotelData = new App\front\Hotel();
$hotel = $hotelData->singleHotel($_GET['id']);
?>

<!--visit places strat here-->
<div class="container">

    <div class="visit-top">
        <h3 style="margin: 20px 0;">Tourist places in Bangladesh</h3>
    </div>
    <div class="text-center">
        <div class="row">
            <div class="col-md-12" align="center">
                <img width="600px" src="view/admin/uploads/hotel/<?php echo  $hotel['image'];?>" class="img-responsive" alt="no image" />
            </div>

            <div class="col-md-12">


                <h2 style="margin: 15px 0";><?php echo  $hotel['name'];?></h2>
                <p style="font-weight: normal;font-size: 12px;color: #333;"><?php echo  $hotel['content'];?></p>
                <span class="#">Category: <?php echo  $hotel['cat'];?></span>


            </div>

        </div>
    </div>




</div>
<?php
include_once '../includes/footer.php';

?>

