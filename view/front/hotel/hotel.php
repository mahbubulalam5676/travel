<?php
include_once '../includes/header.php';
include_once '../../../vendor/autoload.php';
error_reporting(0);

$hotelData = new App\front\Hotel();
$hotel = $hotelData->selectHotel();
?>

<!--visit places strat here-->
<div class="visit">
    <div class="container">
        <div class="visit-main wow bounceInRight" data-wow-delay="0.3s">
            <div class="visit-top">
                <h3>Hotel Place</h3>
                <span class="lft-bar-visit"> </span>
                <span class="rit-bar-visit"> </span>
            </div>
            <div class="visit-bottom">
                <?php
                foreach ($hotel as $hotels){


                    ?>
                    <div class="col-md-4 visit-grid">
                        <div class="item-1 item-type-spin">
                            <a class="item-hover swipebox" href="view/front/hotel/single.php?id=<?=$hotels['unique_id'];?>"  data-title="Vigor">
                                <div class="item-info">
                                    <div class="headline">
                                        Hotels for stay
                                        <div class="line"></div>
                                        <div class="date">Five Star</div>
                                    </div>
                                </div>
                                <div class="mask"></div>
                            </a>
                            <div class="item-img">
                                <img width="420px" src="view/admin/uploads/hotel/<?php echo  $hotels['image']; //"fc31335f.jpg"; ?>" class="img-responsive" alt="no image" />
                            </div>
                        </div>
                        <h4><a href="view/front/hotel/single.php?id=<?=$hotels['unique_id'];?>">Hotels for stay in Dhaka</a></h4>
                        <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain.</p>
                        <!--<div class="visit-blog">
                      <div class="visit-btn">
                         <a href="view/front/hotel/hotel.php">Read More</a>
                      </div>
                      <div class="visit-likes">
                       <a href="#"><span class="#">2k</span></a>
                      </div>
                     <div class="clearfix"> </div>
                   </div>-->
                    </div>

                <?php } ?>

                <!-- <div class="col-md-4 visit-grid">
                          <div class="item-1 item-type-spin">
                                    <a class="item-hover swipebox" href="single.html"  data-title="Vigor">
                                            <div class="item-info">
                                            <div class="headline">
                                                Hotels for stay
                                                <div class="line"></div>
                                                <div class="date">Five Star</div>
                                            </div>
                                        </div>
                                        <div class="mask"></div>
                                    </a>
                                <div class="item-img">
                                <img src="assets/front/images/v2.jpg" class="img-responsive" alt="" />
                                </div>
                    </div>
                          <h4><a href="single.html">Hotels for stay in Caxbazar</a></h4>
                          <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain.</p>
                         <div class="visit-blog">
                       <div class="visit-btn">
                          <a href="single.html">Read More</a>
                       </div>
                       <div class="visit-likes">
                        <a href="#"><span class="#">2k</span></a>
                       </div>
                      <div class="clearfix"> </div>
                    </div>
                 </div>
                 <div class="col-md-4 visit-grid">
                          <div class="item-1  item-type-spin">
                                    <a class="item-hover swipebox" href="single.html"  data-title="Vigor">
                                            <div class="item-info">
                                            <div class="headline">
                                                Hotels for stay
                                                <div class="line"></div>
                                                <div class="date">Five Star</div>
                                            </div>
                                        </div>
                                        <div class="mask"></div>
                                    </a>
                                <div class="item-img">
                                <img src="assets/front/images/v3.jpg" class="img-responsive" alt="" />
                                </div>
                    </div>
                          <h4><a href="single.html">Hotels for stay in Chitagang</a></h4>
                          <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain.</p>
                         <div class="visit-blog">
                       <div class="visit-btn">
                          <a href="single.html">Read More</a>
                       </div>
                       <div class="visit-likes">
                        <a href="#"><span class="#">2k</span></a>
                       </div>
                      <div class="clearfix"> </div>
                    </div>
                 </div>-->
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--visit places end here-->

<?php
include_once '../includes/footer.php';

?>
