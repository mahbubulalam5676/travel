<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title>Tour a Travel : Bangladesh</title>
    <base href="http://localhost/Travel/">

    <link href="assets/front/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/front/js/jquery-1.11.0.min.js"></script>
    <!-- Custom Theme files -->
    <link href="assets/front/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="assets/front/css/style1.css" rel="stylesheet" type="text/css" media="all" />
    <!-- Custom Theme files -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Tour Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--Google Fonts-->
    <link href='//fonts.googleapis.com/css?family=Lato:400,300,700,900' rel='stylesheet' type='text/css'>
    <!-- //end-smoth-scrolling -->
    <script src="assets/front/js/responsiveslides.min.js"></script>
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {
            $("#slider2").responsiveSlides({
                auto: true,
                pager: true,
                speed: 300,
                namespace: "callbacks",
            });
        });
    </script>
    <!-- requried-jsfiles-for owl -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <script src="js/owl.carousel.js"></script>
    <script>
        $(document).ready(function() {
            $("#owl-demo").owlCarousel({
                items : 3,
                lazyLoad : true,
                autoPlay : false,
                pagination : true,
            });
        });
    </script>
    <!-- //requried-jsfiles-for owl -->
    <!-- animated-css -->
    <link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
    <script src="js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <!-- animated-css -->
</head>
<body>
<!--header start here-->
<div class="banner">
    <div class="header">
        <div class="container">
            <div class="header-main">
                <div class="logo wow bounceInLeft" data-wow-delay="0.3s">
                    <a href="index.php"><img src="assets/front/images/logo1.png"  alt=""></a>
                </div>
                <div class="top-navg">
                    <span class="menu"> <img src="assets/front/images/icon.png" alt=""/></span>
                    <nav class="cl-effect-13">
                        <ul class="res">
                            <li><a class="active" href="index.php">Home</a></li>
                            <li><a href="view/front/about.php">About</a></li>
                            <li><a href="view/front/hotel/hotel.php">Hotel</a></li>
                            <li><a href="view/front/zones/place.php">Place</a></li>
                            <li><a href="view/front/gallery.php">Gallery</a></li>
                            <li><a href="view/front/contact.php">Contact</a></li>
                        </ul>
                    </nav>
                    <!-- script-for-menu -->
                    <script>
                        $( "span.menu" ).click(function() {
                            $( "ul.res" ).slideToggle( 300, function() {
                                // Animation complete.
                            });
                        });
                    </script>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <div class="banner-main">
        <div class="slider-bann wow bounceInRight" data-wow-delay="0.3s">
            <ul class="rslides" id="slider2">
                <li>
                    <h3>Welcome To Our Travel</h3>
                    <h4>The Best Way To Be Lost</h4>
                    <div class="ban-btn">
                        <a href="index.php" class="hvr-bounce-out">Load More</a>
                    </div>
                </li>
                <li>
                    <h3>Find Beautiful Place in Bangladesh</h3>
                    <h4>The Best Way To Be Lost</h4>
                    <div class="ban-btn">
                        <a href="index.php" class="hvr-bounce-out">Load More</a>
                    </div>
                </li>
                <li>
                    <h3>To travel is to live</h3>
                    <h4>The Best Way To Be Lost</h4>
                    <div class="ban-btn">
                        <a href="index.php" class="hvr-bounce-out">Load More</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!--header end here-->