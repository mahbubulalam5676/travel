<?php
    session_start();
    include_once '../../../vendor/autoload.php';

   /*
    $auth = new App\admin\Auth\Auth();

    if(isset($_POST['submit'])){
        $email = trim($_POST['email']);
        $email = htmlspecialchars($email);
        $pass  = trim($_POST['password']);
        $pass = htmlspecialchars($pass);

        $auth->login($email, $pass);

    }*/


if(isset($_POST['submit'])){
    $auth = new App\admin\Auth\Auth();
    $data = $auth->login2();
    foreach($data as $user) {
        if ($_POST['email'] == $user['email'] && $_POST['password'] == $user['password']) {
            $_SESSION['user'] = '';
            $_SESSION['insert'] = "Successfully Log in";
            header('location: ../index.php');
        }
    }
}


    /*if(isset($_POST['submit'])){
        $data  = $auth->validate($_POST);
        $result = $auth->store($data);
    }*/

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Backbenchers</title>

    <base href="http://localhost/Travel/">

    <!-- Bootstrap -->
    <link href="assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="assets/admin/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="assets/admin/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="assets/admin/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="assets/admin/build/css/custom.min.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <div class="row" style="padding-top: 122px; width: 100%">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please Sign In</h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="" method="post">
                        <fieldset>

                            <div class="form-group">
                                <label for="">Email: </label>
                                <input class="form-control" placeholder=" Email..." name="email" type="text" value="">
                            </div>
                            <div class="form-group">
                                <label for="">Password: </label>
                                <input class="form-control" placeholder="Password..." name="password" type="password" value="">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                </label>
                            </div>
                            <!-- Change this to a button or input when using this as a form -->
                            <button type="submit" name="submit" class="btn btn-lg btn-success btn-block">Log In</button>
                            <p>New User !<a href="view/admin/auth/register.php" "> <strong> Sign Up</strong></a></p>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
