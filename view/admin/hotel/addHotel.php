<?php include_once '../includes/header.php'; ?>



<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">



        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-8 col-md-offset-2">

                    <form action="view/admin/hotel/store.php" method="post" enctype="multipart/form-data" id="demo-form" data-parsley-validate="" novalidate="">
                        <div class="title_left">
                            <h3>Hotel</h3>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="fullname">Name :</label>
                            <input type="text" id="fullname" class="form-control" name="name" placeholder="Enter hotel name..." required="" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="cat">Category :</label>
                            <select name="cat" id="" class="form-control">
                                <option>Select One</option>
                                <option value="FiveStar">5 Star</option>
                                <option value="ThreeStar">3 Star</option>
                                <option value="TwoStar">2 Star</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="message">Content:</label>
                            <textarea name="content" class="resizable_textarea form-control" placeholder="Enter your content of this place..." autocomplete="off">

                            </textarea>
                        </div>

                        <div class="form-group">
                            <label for="">Upload Image</label>
                            <input type="file" name="image" >
                        </div>

                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="reset" class="btn btn-primary">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php include_once '../includes/footer.php'; ?>
