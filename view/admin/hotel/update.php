<?php
/**
 * Created by PhpStorm.
 * User: Shobuj
 * Date: 03-Oct-17
 * Time: 4:15 PM
 */

include_once '../../../vendor/autoload.php';

$hotel = new App\admin\hotel\Hotel();

if(!empty($_FILES['image']['name'])){
    $hotel->imageUnlink($_POST['id']);
    $_POST['image'] = $hotel->upload();
}

$hotel->set($_POST);
$hotel->update();


