<?php
/**
 * Created by PhpStorm.
 * User: Shobuj
 * Date: 03-Oct-17
 * Time: 2:55 PM
 */
include_once '../includes/header.php';

include_once '../../../vendor/autoload.php';

$hotel = new \App\admin\hotel\Hotel();

$data = $hotel->selectSingleHotel($_GET['view']);

?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">

                <div class="x_panel">
                    <div class="x_title">
                        <h3>Single view Image<small></small></h3>

                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <div class="row">


                            <div class="col-md-55">
                                <div class="thumbnail2">
                                    <div class="image view view-first">
                                        <img style="width: 200px; display: block;" src="view/admin/uploads/hotel/<?php echo $data['image']; ?>" alt="image">
                                        <div class="mask">
                                            <p><?php echo $data['title']; ?></p>

                                        </div>
                                    </div>
                                    <div class="caption">
                                        <h3><?php echo $data['cat']; ?></h3>
                                    </div>
                                    <div>
                                        <h4><?php echo $data['content']; ?></h4>
                                    </div>

                                </div>
                            </div>



                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<!-- footer content -->
<?php include_once '../includes/footer.php'; ?>
