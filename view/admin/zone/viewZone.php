<?php
    include_once '../includes/header.php';
    include_once '../../../vendor/autoload.php';
?>

<?php
if(!isset($_SESSION)){
    session_start();
}

$zone = new App\admin\zone\Zone();

$data = $zone->selectZone();


?>

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Travel Area</h3>
                </div>



                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                        </div>
                    </div>
                </div>
            </div>



            <div class="clearfix"></div>

            <div style="position: fixed;z-index: 111;">
                <?php
                if(isset($_SESSION['insert'])){
                    echo "<dive class='alert alert-success'>".$_SESSION['insert']."</dive>";
                    session_unset();
                }
                if(isset($_SESSION['update'])){
                    echo "<dive class='alert alert-success'>".$_SESSION['update']."</dive>";
                    session_unset();
                }
                if(isset($_SESSION['delete'])){
                    echo "<dive class='alert alert-danger'>".$_SESSION['delete']."</dive>";
                    session_unset();
                }
                ?>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Media Gallery <small></small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <div class="row">

                                <p>Media gallery design emelents</p>


                                    <?php
                                    $i=0;
                                    foreach($data as $result){
                                    $i++;

                                    ?><div class="col-md-55">
                                    <div class="thumbnail">
                                        <div class="image view view-first">
                                            <img style="width: 100%; display: block;" src="view/admin/uploads/<?php echo $result['image']; ?>" alt="image">
                                            <div class="mask">
                                                <p><?php echo $result['title']; ?></p>
                                                <div class="tools tools-bottom">
                                                    <a href="#"><i class="fa fa-link"></i></a>
                                                    <a href="#"><i class="fa fa-pencil"></i></a>
                                                    <a href="#"><i class="fa fa-times"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="caption">
                                            <p><?php echo $result['cat']; ?></p>
                                        </div>
                                        <div>
                                            <a  class="text-primary"   href="view/admin/zone/singleView.php?view=<?php echo $result['uniqu_id']; ?>">View</a> |
                                            <a class="text-info" href="view/admin/zone/edit.php?edit=<?php echo $result['uniqu_id']; ?>">Edit</a> |
                                            <a data-toggle="modal" data-target=".bs-example-modal-sm" class="text-danger delete" data-id="<?php echo $result['uniqu_id']; ?>" href="javascript:void(0)">Delete</a>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <!-- Large modal -->

                                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-sm">
                                        <form action="view/admin/zone/tmp_delete.php" method="get">
                                            <input type="hidden"  name="id" id="delete">
                                            <div class="modal-content">

                                                <div class="modal-header">

                                                    <h4 class="modal-title" id="myModalLabel2">Are you sure want to delete ?</h4>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!--<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-sm">
                                    <form action="view/admin/zone/modal.php" method="get">
                                        <input type="hidden"  name="id" id="view">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                </button>
                                                <img style="width: 100%; display: block;" src="view/admin/uploads/<?php /*echo $data['image']; */?>" alt="image">
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

    <!-- footer content -->
<?php include_once '../includes/footer.php'; ?>