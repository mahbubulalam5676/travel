<?php
    include_once '../includes/header.php';
    include_once '../../../vendor/autoload.php';

    $zone = new \App\admin\zone\Zone();

    $data = $zone->selectSingleZone($_GET['edit']);


?>



<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">



        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-8 col-md-offset-2">

                    <form action="view/admin/zone/update.php" method="post" enctype="multipart/form-data" id="demo-form" data-parsley-validate="" novalidate="">
                        <div class="title_left">
                            <h3>Travel Zone for Bangladesh</h3>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="fullname">Title :</label>
                            <input type="text" id="fullname" class="form-control" name="title" value="<?php echo $data['title']; ?>" placeholder="Enter title..." required="" autocomplete="off">
                            <input type="hidden" id="id" class="form-control" name="id" value="<?php echo $data['uniqu_id']; ?>"  required="" autocomplete="off">
                            <input type="hidden" id="id" class="form-control" name="image" value="<?php echo $data['image']; ?>"  required="" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="cat">Category :</label>
                            <select name="cat" id="" class="form-control">
                                <option>Select One</option>
                                <option <?php  echo ($data['cat']=='Beach')?'selected':'' ?> value="Beach">Beach</option>
                                <option <?php  echo ($data['cat']=='Mountain')?'selected':'' ?> value="Mountain">Mountain</option>
                                <option <?php  echo ($data['cat']=='Spectacular')?'selected':'' ?> value="Spectacular">Spectacular</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="message">Content:</label>
                            <textarea name="content"  class="resizable_textarea form-control" placeholder="Enter your content of this place..." style="margin: 0px 155.75px 0px 0px; width: 699px; height: 109px; z-index: auto; position: relative; line-height: 20px; font-size: 14px; transition: none; background: transparent !important;" data-gramm="true" data-txt_gramm_id="b63c8e4c-0b79-7ca4-6ee7-42ce6357dfa7" data-gramm_id="b63c8e4c-0b79-7ca4-6ee7-42ce6357dfa7" spellcheck="false" data-gramm_editor="true" autocomplete="off">
                                <?php echo $data['content']; ?>
                            </textarea>
                        </div>

                        <div class="form-group">
                            <label for="">Upload Image</label>
                            <input type="file" name="image" >
                        </div>

                        <div style="padding-bottom: 20px">
                            <img width="200px" src="view/admin/uploads/<?php echo $data['image']; ?>" alt="">
                        </div>

                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="reset" class="btn btn-primary">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php include_once '../includes/footer.php'; ?>
