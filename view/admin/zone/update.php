<?php
/**
 * Created by PhpStorm.
 * User: Shobuj
 * Date: 03-Oct-17
 * Time: 4:15 PM
 */

include_once '../../../vendor/autoload.php';

$zone = new App\admin\zone\Zone();

if(!empty($_FILES['image']['name'])){
    $zone->imageUnlink($_POST['id']);
    $_POST['image'] = $zone->upload();
}

$zone->set($_POST);
$zone->update();


