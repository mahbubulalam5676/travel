<!-- footer content -->
<footer>
    <div class="pull-right">
        Team of Backbenchers, by <a href="https://backbencher.com">Backbencher</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>

<!-- jQuery -->
<script src="assets/admin/vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="assets/admin/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="assets/admin/vendors/nprogress/nprogress.js"></script>

<!-- iCheck -->
<script src="assets/admin/vendors/iCheck/icheck.min.js"></script>
<!-- Datatables -->
<script src="assets/admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="assets/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="assets/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="assets/admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
<script src="assets/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="assets/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="assets/admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="assets/admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
<script src="assets/admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="assets/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
<script src="assets/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
<script src="assets/admin/vendors/jszip/dist/jszip.min.js"></script>
<script src="assets/admin/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="assets/admin/vendors/pdfmake/build/vfs_fonts.js"></script>

<!-- Custom Theme Scripts -->
<script src="assets/admin/build/js/custom.min.js"></script>
<script>
    $(document).ready(function() {
        $('.alert').delay(2000).fadeOut(1500,function () {
            $(this).alert('close');
        });
    });

    $(document).on('click','.delete',function () {
        var id = $(this);
        $('#delete').val(id.data('id'));
    });
   /* $(document).on('click','.view',function () {
        var id = $(this);
        $('#view').val(id.data('id'));
    });*/
</script>
</body>
</html>
