<?php include_once '../includes/header.php'; ?>
<?php
include_once '../../../vendor/autoload.php';
$objAdmin = new \App\admin\admin\Admin();
$admin = $objAdmin->single($_GET['id']);
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Admin Profile</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">

                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table id="datatable-buttons" class="table table-striped table-bordered">

                            <tr>
                                <th>Name</th>
                                <td><?=$admin['name']?></td>
                            </tr>
                           <tr>
                                <th>E-mail</th>
                                <td><?=$admin['email']?></td>
                            </tr>
                           <tr>
                                <th>User Role</th>
                                <td><?=$admin['role_name']?></td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td><?=$admin['country']?></td>
                            </tr>
                           <tr>
                                <th>Status</th>
                                <td><?=($admin['active']==1)? "Active":"Deactive";?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once '../includes/footer.php'; ?>
