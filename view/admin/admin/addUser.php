<?php include_once '../includes/header.php'; ?>
<?php
include_once '../../../vendor/autoload.php';
$admin_role = new \App\admin\admin\Admin();
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">



        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-8 col-md-offset-2">

                    <form action="view/admin/admin/store.php" method="post" id="demo-form" data-parsley-validate="" novalidate="">
                        <div class="title_left">
                            <h3>Add admin panel user</h3>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="fullname">Name :</label>
                            <input type="text" id="fullname" class="form-control" name="name" placeholder="Enter name..." required="" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail :</label>
                            <input type="email" id="email" class="form-control" name="email" placeholder="Enter email..." required="" autocomplete="off">
                        </div>

                         <div class="form-group">
                            <label for="password">Password :</label>
                            <input type="password" id="password" class="form-control" name="password" placeholder="Enter password..." required="" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="cat">User Role :</label>
                            <select name="role_id" id="" class="form-control">
                                <option>Select One</option>
                                <?php $admin_role = $admin_role->Admin_Role();
                                    foreach ($admin_role as $role){
                                ?>
                                <option value="<?=$role['role_id'];?>"><?=$role['role_name'];?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <!--<div class="form-group">
                            <label for="">Upload Image</label>
                            <input type="file" name="image" >
                        </div>-->

                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="reset" class="btn btn-primary">Reset</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->


<?php include_once '../includes/footer.php'; ?>
