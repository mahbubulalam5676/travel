<?php include_once '../includes/header.php'; ?>
<?php
include_once '../../../vendor/autoload.php';
$objAdmin = new \App\admin\admin\Admin();
?>
<style>
    .alert{
        padding: 10px 0;background-color: #66B267;color: #fff;font-size: 18px;z-index: 222;font-weight: 700;
    }
</style>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <?php if(isset($_SESSION['insert']) || isset($_SESSION['delete']) || isset($_SESSION['update'])){ ?>
            <div class="row">
                <div class="col-md-12" align="center">
                    <p class="alert">
                        <?php    if(isset($_SESSION['insert'])) {
                            echo $_SESSION['insert'];
                            unset($_SESSION['insert']);
                        }if(isset($_SESSION['delete'])) {
                            echo $_SESSION['delete'];
                            unset($_SESSION['delete']);
                        }if(isset($_SESSION['update'])) {
                            echo $_SESSION['update'];
                            unset($_SESSION['update']);
                        }
                        ?>
                    </p>
                </div>
            </div>
        <?php }?>
        <div class="page-title">
            <div class="title_left">
                <h3>Admin Profile</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">

                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a>
                                    </li>
                                    <li><a href="#">Settings 2</a>
                                    </li>
                                </ul>
                            </li>
                            <li><a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">

                        <table id="datatable-buttons" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>S.N</th>
                                <th>Name</th>
                                <th>E-mail</th>
                                <th>User Role</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $admin_data = $objAdmin->Admin();
                            $i=0;
                            foreach ($admin_data as $admin){
                                ?>
                                <tr>
                                    <td><?=++$i;?></td>
                                    <td><?=$admin['name']?></td>
                                    <td><?=$admin['email']?></td>
                                    <td><?=$admin['role_name']?></td>
                                    <td><?=($admin['active']==1)? "Active":"Deactive";?></td>
                                    <td>
                                        <a href="view/admin/admin/single.php?id=<?=$admin['unique_id']?>"><i class="fa fa-plus"></i></a>
                                        <a href="view/admin/admin/edit.php?id=<?=$admin['unique_id']?>"><i class="fa fa-pencil"></i></a>
                                        <a data-toggle="modal" data-target=".bs-example-modal-sm" class="text-danger delete" data-id="<?php echo $admin['id']; ?>" href="javascript:void(0)">
                                        <i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>

                        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <form action="view/admin/admin/delete.php" method="get">
                                    <input type="hidden"  name="id" id="delete">
                                    <div class="modal-content">

                                        <div class="modal-header">

                                            <h4 class="modal-title" id="myModalLabel2">Are you sure want to delete ?</h4>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include_once '../includes/footer.php'; ?>
