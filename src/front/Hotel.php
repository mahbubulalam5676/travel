<?php
/**
 * Created by PhpStorm.
 * User: Shobuj
 * Date: 05-Oct-17
 * Time: 6:03 PM
 */
namespace App\front;


use App\Connection;
use PDO;
use PDOException;

class Hotel extends Connection
{


    public function selectHotel(){
        try {
            $query = "select * from hotel where delete_at = '1' limit 6";
            $data = $this->con->prepare($query);
            $data->execute();
            return $data->fetchAll(PDO::FETCH_ASSOC);
            //return $data;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }
    public function singleHotel($id){
        try {
            $query = "select * from hotel where unique_id = '$id'";
            $data = $this->con->prepare($query);
            $data->execute();
            return $data->fetch(PDO::FETCH_ASSOC);
            //return $data;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

}

