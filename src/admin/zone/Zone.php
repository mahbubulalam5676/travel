<?php
/**
 * Created by PhpStorm.
 * User: Shobuj
 * Date: 03-Oct-17
 * Time: 11:53 AM
 */

namespace App\admin\zone;


    if(!isset($_SESSION)){
        session_start();
    }



use App\Connection;
use PDO;
use PDOException;

class Zone extends Connection
{

    private $title;
    private $cat;
    private $content;
    private $image;
    private $id;


    public function set($data = array())
    {
        if (array_key_exists('title', $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists('cat', $data)) {
            $this->cat = $data['cat'];
        }
        if (array_key_exists('content', $data)) {
            $this->content = $data['content'];
        }
        if (array_key_exists('image', $data)) {
            $this->image = $data['image'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function upload()
    {
        $img_name = $_FILES['image']['name'];
        $tmp_name = $_FILES['image']['tmp_name'];

        $gen_name = substr(md5(time()), 0, 8);
        $ext_name = end(explode('.', $img_name));

        $_POST['image'] = $gen_name . '.' . $ext_name;
        if ($_POST['image']) {
            move_uploaded_file($tmp_name, '../uploads/' . $_POST['image']);
            return $_POST['image'];
        }
    }

    public function store()
    {
        try {
            $query = "insert into zone(title, cat, content, image, uniqu_id, delete_at) values(:title, :cat, :content, :image, :uniqu_id, :delete_at)";
            $data = $this->con->prepare($query);
            $data->bindValue(':title', $this->title, PDO::PARAM_STR);
            $data->bindValue(':cat', $this->cat, PDO::PARAM_STR);
            $data->bindValue(':content', $this->content, PDO::PARAM_STR);
            $data->bindValue(':image', $this->image, PDO::PARAM_STR);
            $data->bindValue(':uniqu_id', md5(time()), PDO::PARAM_STR);
            $data->bindValue(':delete_at', 1, PDO::PARAM_INT);
            $result = $data->execute();

            if ($result) {
                $_SESSION['insert'] = "Data inserted Successfully !!";
                header("location:viewZone.php");
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br>";
            die();
        }
    }

    //select all data from zone table...
    public function selectZone()
    {
        try {
            $query = "select * from zone where delete_at = '1'";
            $data = $this->con->prepare($query);
            $data->execute();
            return $data->fetchAll(PDO::FETCH_ASSOC);
            //return $data;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

    public function selectSingleZone($id)
    {
        try {
            $query = "select * from zone where uniqu_id=:id";
            $data = $this->con->prepare($query);
            $data->bindValue(':id', $id, PDO::FETCH_ASSOC);
            $data->execute();
            return $data->fetch(PDO::FETCH_ASSOC);
            //return $data;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

    public function imageUnlink($id)
    {
        try {
            $query = "select image from zone where uniqu_id=:id";
            $data = $this->con->prepare($query);
            $data->bindValue(':id', $id, PDO::FETCH_ASSOC);
            $data->execute();
            $data = $data->fetch(PDO::FETCH_ASSOC);

            if (isset($data['image'])) {
                unlink('../uploads/' . $data['image']);
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

    public function imageDelete($id)
    {
        try {
            $query = "delete from zone where uniqu_id=:id";
            $data = $this->con->prepare($query);
            $data->bindValue(':id', $id, PDO::FETCH_ASSOC);
            $result = $data->execute();

            if ($result) {
                $_SESSION['delete'] = "Data Deleted successfully !!";
                header("location:trash.php");
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

    public function update()
    {
        try {
            $query = "update zone set  
                    title = :title,
                    cat   = :cat,
                    content = :content,
                    image   = :image
                    where uniqu_id = :id ";

            $data = $this->con->prepare($query);
            $data->bindValue(':title', $this->title, PDO::FETCH_ASSOC);
            $data->bindValue(':cat', $this->cat, PDO::FETCH_ASSOC);
            $data->bindValue(':content', $this->content, PDO::FETCH_ASSOC);
            $data->bindValue(':image', $this->image, PDO::FETCH_ASSOC);
            $data->bindValue(':id', $this->id, PDO::FETCH_ASSOC);
            $result = $data->execute();

            if ($result) {
                $_SESSION['update'] = "Data Updated successfully !!";
                header("location:viewZone.php");
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

    public function tmp_delete($id)
    {
        try {
            $stm = "update zone set delete_at=2 where uniqu_id=:id";
            $stm = $this->con->prepare($stm);
            //$stm = $this->con->prepare("UPDATE `zone` SET `delete_at` = '2' WHERE unique_id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();

            if ($stm) {
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:viewZone.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function trash(){
        try {
            $query = "select * from zone where delete_at = '2'";
            $data = $this->con->prepare($query);
            $data->execute();
            return $data->fetchAll(PDO::FETCH_ASSOC);
            //return $data;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

    public function restore($id){
        try {
            $stm = "update zone set delete_at=1 where uniqu_id=:id";
            $stm = $this->con->prepare($stm);
            //$stm = $this->con->prepare("UPDATE `zone` SET `delete_at` = '2' WHERE unique_id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();

            if ($stm) {
                $_SESSION['insert'] = 'Data successfully Restored !!!';
                header('location:../trash.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}