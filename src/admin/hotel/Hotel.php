<?php
/**
 * Created by PhpStorm.
 * User: Shobuj
 * Date: 03-Oct-17
 * Time: 11:53 AM
 */

namespace App\admin\Hotel;


if(!isset($_SESSION)){
    session_start();
}



use App\Connection;
use PDO;
use PDOException;

class Hotel extends Connection
{

    private $name;
    private $cat;
    private $content;
    private $image;
    private $id;


    public function set($data = array())
    {
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('cat', $data)) {
            $this->cat = $data['cat'];
        }
        if (array_key_exists('content', $data)) {
            $this->content = $data['content'];
        }
        if (array_key_exists('image', $data)) {
            $this->image = $data['image'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function upload()
    {
        $img_name = $_FILES['image']['name'];
        $tmp_name = $_FILES['image']['tmp_name'];

        $gen_name = substr(md5(time()), 0, 8);
        $ext_name = end(explode('.', $img_name));

        $_POST['image'] = $gen_name . '.' . $ext_name;
        if ($_POST['image']) {
            move_uploaded_file($tmp_name, '../uploads/hotel/' . $_POST['image']);
            return $_POST['image'];
        }
    }

    public function store()
    {
        try {
            $query = "insert into hotel(name, cat, content, image, unique_id, delete_at) values(:name, :cat, :content, :image, :unique_id, :delete_at)";
            $data = $this->con->prepare($query);
            $data->bindValue(':name', $this->name, PDO::PARAM_STR);
            $data->bindValue(':cat', $this->cat, PDO::PARAM_STR);
            $data->bindValue(':content', $this->content, PDO::PARAM_STR);
            $data->bindValue(':image', $this->image, PDO::PARAM_STR);
            $data->bindValue(':unique_id', md5(time()), PDO::PARAM_STR);
            $data->bindValue(':delete_at', 1, PDO::PARAM_INT);
            $result = $data->execute();

            if ($result) {
                $_SESSION['insert'] = "Data inserted Successfully !!";
                header("location:view.php");
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br>";
            die();
        }
    }

    public function selectZone(){
        try {
            $query = "select * from hotel where delete_at = '1'";
            $data = $this->con->prepare($query);
            $data->execute();
            return $data->fetchAll(PDO::FETCH_ASSOC);
            //return $data;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

    public function selectSingleHotel($id){
        try {
            $query = "select * from hotel where unique_id=:id";
            $data = $this->con->prepare($query);
            $data->bindValue(':id', $id, PDO::FETCH_ASSOC);
            $data->execute();
            return $data->fetch(PDO::FETCH_ASSOC);
            //return $data;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

    public function imageUnlink($id)
    {
        try {
            $query = "select image from hotel where unique_id=:id";
            $data = $this->con->prepare($query);
            $data->bindValue(':id', $id, PDO::FETCH_ASSOC);
            $data->execute();
            $data = $data->fetch(PDO::FETCH_ASSOC);

            if (isset($data['image'])) {
                unlink('../uploads/hotel/' . $data['image']);
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

    public function imageDelete($id)
    {
        try {
            $query = "delete from hotel where unique_id=:id";
            $data = $this->con->prepare($query);
            $data->bindValue(':id', $id, PDO::FETCH_ASSOC);
            $result = $data->execute();

            if ($result) {
                $_SESSION['delete'] = "Data Deleted successfully !!";
                header("location:view.php");
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

    public function update(){
        try {
            $query = "update hotel set  
                    name = :name,
                    cat   = :cat,
                    content = :content,
                    image   = :image
                    where unique_id = :id ";

            $data = $this->con->prepare($query);
            $data->bindValue(':name', $this->name, PDO::FETCH_ASSOC);
            $data->bindValue(':cat', $this->cat, PDO::FETCH_ASSOC);
            $data->bindValue(':content', $this->content, PDO::FETCH_ASSOC);
            $data->bindValue(':image', $this->image, PDO::FETCH_ASSOC);
            $data->bindValue(':id', $this->id, PDO::FETCH_ASSOC);
            $result = $data->execute();

            if ($result) {
                $_SESSION['update'] = "Data Updated successfully !!";
                header("location:view.php");
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

    public function tmp_delete($id)
    {
        try {
            $stm = "update hotel set delete_at=2 where unique_id=:id";
            $stm = $this->con->prepare($stm);
            //$stm = $this->con->prepare("UPDATE `zone` SET `delete_at` = '2' WHERE unique_id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();

            if ($stm) {
                $_SESSION['delete'] = 'Data successfully Deleted !!!';
                header('location:view.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function hotelTrash(){
        try {
            $query = "select * from hotel where delete_at = '2'";
            $data = $this->con->prepare($query);
            $data->execute();
            return $data->fetchAll(PDO::FETCH_ASSOC);
            //return $data;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage() . "<br />";
            die();
        }
    }

    public function restore($id){
        try {
            $stm = "update hotel set delete_at=1 where unique_id=:id";
            $stm = $this->con->prepare($stm);
            //$stm = $this->con->prepare("UPDATE `zone` SET `delete_at` = '2' WHERE unique_id = :id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();

            if ($stm) {
                $_SESSION['insert'] = 'Data successfully Restored !!!';
                header('location:../Trash/hotelTrash.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

}