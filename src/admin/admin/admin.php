<?php
/**
 * Created by PhpStorm.
 * User: Mahbubul Alam
 * Date: 10/3/2017
 * Time: 5:36 PM
 */

namespace App\admin\admin;
if(!isset($_SESSION)){
    session_start();
}
use App\Connection;
use PDOException;
use PDO;
class Admin extends Connection
{
    private $name;
    private $email;
    private $password;
    private $address;
    private $country;
    private $role_id;
    private $id;
    public function Set($data = array()){
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('password',$data)){
            $this->password=md5($data['password']);
        }
        if(array_key_exists('address',$data)){
            $this->address=$data['address'];
        }
        if(array_key_exists('country',$data)){
            $this->country=$data['country'];
        }
        if(array_key_exists('role_id',$data)){
            $this->role_id=$data['role_id'];
        }
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }

    }
    public function Admin_Add(){
        try {

            $stm =  $this->con->prepare("INSERT INTO `admin`(`name`,`email`,`password`, `role_id`, `unique_id`) 
                                        VALUES(:name,:email,:password, :role_id, :unique_id)");
            $result =$stm->execute(array(
                ':name' => $this->name,
                ':email' => $this->email,
                ':password' => $this->password,
                ':role_id' => $this->role_id,
                ':unique_id' => md5(time()).rand(100000,9000000)
            ));
            if($result){
                $_SESSION['insert'] = 'Data successfully Inserted !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function Admin(){
        try {
            $stmt =  $this->con->prepare("SELECT admin.*,admin_role.role_name FROM `admin`,`admin_role` WHERE admin_role.role_id=admin.role_id");
            $stmt->execute();
            if($stmt) {
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }else{
                echo "Error";
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function single($id){
        try {

            $stm =  $this->con->prepare("SELECT admin.*,admin_role.role_name FROM `admin`,`admin_role` WHERE admin.unique_id = :id AND admin_role.role_id=admin.role_id");
            $stm->bindValue(':id', $id, PDO::PARAM_STR);
            $stm->execute();
            if($stm){
                return $stm->fetch(PDO::FETCH_ASSOC);
            }else{
                echo "Error";

            }


        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function Admin_Update(){
        try {

            $stmt = $this->con->prepare("UPDATE `admin` SET `name` = :name, `country` = :country, `role_id` = :role_id, `email` = :email WHERE `admin`.`unique_id` = :id;");
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':country', $this->country, PDO::PARAM_STR);
            $stmt->bindValue(':role_id', $this->role_id, PDO::PARAM_INT);
            $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!!';
                header('location:index.php');
            }

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function Admin_Role(){
        try {
            $stmt =  $this->con->prepare("SELECT * FROM `admin_role` WHERE active=1");
            $stmt->execute();
            if($stmt) {
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }else{
                echo "Error";
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function login($data){
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('password',$data)){
            $this->password=md5($data['password']);
        }
        try {
            $stmt =  $this->con->prepare("SELECT admin.*,admin_role.role_name FROM `admin`,`admin_role` WHERE admin_role.role_id=admin.role_id AND email=:email AND password=:password");
            $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindValue(':password', $this->password, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt->rowCount()>0) {
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                $_SESSION['unique_id'] = $result['unique_id'];
                $_SESSION['name'] = $result['name'];
                $_SESSION['email'] = $result['email'];
                $_SESSION['role_id'] = $result['role_id'];
                $_SESSION['role_name'] = $result['role_name'];
                header("Location:index.php");
            }else{
                echo "Error";
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }

    }
}