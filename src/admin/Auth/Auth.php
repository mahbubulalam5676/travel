<?php
/**
 * Created by PhpStorm.
 * User: Shobuj
 * Date: 06-Oct-17
 * Time: 1:35 AM
 */

namespace App\admin\Auth;


use App\Connection;
use PDO;
use PDOException;

class Auth extends Connection
{

    private $name;
    private $email;
    private $password;

    public function validate($data = array()){
        if(array_key_exists('name', $data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('email',$data)){
            $this->email = $data['email'];
        }
        if(array_key_exists('password', $data)){
            $this->password = $data['password'];
         }

         return $this;
    }

    //insert data into database
    public function store(){
        try{
            $query = "insert into users(name, email, password) values(:name, :email, :password)";
            $stmt = $this->con->prepare($query);
            $stmt->bindValue(':name', $this->name, PDO::PARAM_STR);
            $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindValue(':password', $this->password, PDO::PARAM_STR);
            $data = $stmt->execute();

            if($data){
                header("location:login.php");
            }
        }
        catch (PDOException $e){
            echo "Error: ".$e->getMessage()."<br>";
            die();
        }
    }

    //select data for login
    public function login($email, $pass){
       $query = "select * from users where email='$email' and password='$pass'";
       $stmt = $this->con->prepare($query);
       //$stmt->bindValue(':email', $email, PDO::PARAM_STR);
       //$stmt->bindValue(':pass', $pass, PDO::PARAM_STR);
       $data = $stmt->execute();
        //return $data;

       if(($data)){
           $_SESSION['user'] = '';
           header('Location:register.php');
       }
    }

    public function login2(){
        $query = "select * from users";
        $stmt = $this->con->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(PDO::PARAM_STR);
    }
}