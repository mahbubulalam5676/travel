-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 08, 2017 at 12:05 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travel`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(100) NOT NULL,
  `role_id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(40) NOT NULL,
  `address` varchar(60) NOT NULL,
  `country` varchar(20) NOT NULL,
  `about` text NOT NULL,
  `gender` varchar(10) NOT NULL,
  `starting_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deactive_time` datetime NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `unique_id`, `role_id`, `name`, `email`, `password`, `address`, `country`, `about`, `gender`, `starting_date`, `deactive_time`, `active`) VALUES
(1, 'b26872515c171f2dc04b37111f6296be8781940', 1, 'Mahbubul', 'mahbubrahman5676@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', '', '', '', '2017-10-04 00:17:50', '0000-00-00 00:00:00', 1),
(2, '175612ef084f572e5796de1aff2a71c81310045', 5, 'Ripat', 'rifat@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', 'India', '', '', '2017-10-04 00:19:28', '0000-00-00 00:00:00', 1),
(3, '37f2f65208bd1da5ace58dfcbbce9e0d8692189', 4, 'Shahad', 'mahbubrahman5676@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', '', '', '', '2017-10-04 00:20:44', '0000-00-00 00:00:00', 0),
(4, '46bc9bc780c2f493d8392506b60f391d5529927', 2, 'Soboj', 'soboj@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', 'Bangladesh', '', '', '2017-10-04 00:21:13', '0000-00-00 00:00:00', 0),
(5, '91f65bc6cd791dcd4641c95ac8e93a0c5216858', 2, 'Al - Mahfuj', 'mahfuj@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', 'Bangladesh', '', '', '2017-10-04 00:21:56', '0000-00-00 00:00:00', 1),
(6, 'd1ea57473c47de9f73c6e4da576f207c3411402', 4, 'Rafi', 'rafi@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', '', '', '', '', '2017-10-04 13:09:56', '0000-00-00 00:00:00', 0),
(7, '66b149c32341374627d946c037beec5a2779941', 3, 'test', 'test@gmail.com', '202cb962ac59075b964b07152d234b70', '', 'Bangladesh', '', '', '2017-10-06 04:42:12', '0000-00-00 00:00:00', 0),
(8, '1388b0df151200e1977eec40ce374b617945624', 4, 'roni', 'roni@gmail.com', '202cb962ac59075b964b07152d234b70', '', 'Bangladesh', '', '', '2017-10-06 19:21:42', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(40) NOT NULL,
  `date` date NOT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`role_id`, `role_name`, `date`, `active`) VALUES
(1, 'Supper Admin', '2017-10-03', 1),
(2, 'Admin', '2017-10-03', 1),
(3, 'Author', '2017-10-03', 1),
(4, 'Editor', '2017-10-03', 1),
(5, 'Subscriber', '2017-10-03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `cat` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `delete_at` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`id`, `name`, `cat`, `content`, `image`, `unique_id`, `delete_at`) VALUES
(2, 'Sylet', 'ThreeStar', ' Sylet Hotel\r\n                                                        ', 'b42b0248.jpg', '2e9c37604f95fbd84237ec01711c1008', 1),
(3, 'Rajshahi', 'FiveStar', 'Rajshahi Hotel\r\n                            ', '16f3ec73.jpg', '16f3ec73b31a7769fdea82463137c023', 1),
(6, 'asdf', 'ThreeStar', 'asdfsd\r\n                            ', '4cd388e3.jpg', '4cd388e32c901389b2fe35f48cab6da4', 2),
(7, 'ewr', 'ThreeStar', 'werter\r\n                                                        ', '1f406c02.jpg', '1f406c0205d3993aa0b04224c55fc788', 1),
(8, 'Coxbazar Motel', 'FiveStar', 'Coxbazar motel for stay\r\n                            ', '5a945bb2.jpg', '5a945bb2ddeec4f80fd44b6c3d43540b', 1),
(9, 'hotel new ', 'FiveStar', 'hotel now dhaka\r\n                            ', '1455dd9a.jpg', '1455dd9ab1a8f2534328e5940cd5a3b1', 1),
(10, 'hotel test', 'ThreeStar', 'hotel test here\r\n                            ', '489451d6.jpg', '489451d6b24daa841d2f4438d3297772', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(1, 'Shobuj', 'shobuj@gmail.com', '123'),
(2, 'Rana', 'rana@gmail.com', '12345'),
(3, 'shohel', 'shohel@gmail.com', '123'),
(4, 'sa', 'sa@gmail.com', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE `zone` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `cat` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `uniqu_id` varchar(255) NOT NULL,
  `delete_at` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zone`
--

INSERT INTO `zone` (`id`, `title`, `cat`, `content`, `image`, `uniqu_id`, `delete_at`) VALUES
(26, 'Lorem ipsum dolor sit amet', 'Mountain', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias aliquam animi, culpa cum deleniti dolorem doloremque earum error eveniet, excepturi harum magni maxime modi nam nostrum odit omnis, praesentium provident quod recusandae reprehenderit saepe sequi ut voluptas. Ad aliquam, amet aspernatur delectus dolores doloribus eaque error exercitationem explicabo facere nam natus nesciunt odio quibusdam, quisquam rem repudiandae temporibus! Debitis ea enim itaque maiores mollitia necessitatibus perferendis quis rerum ullam, voluptatem! A adipisci alias dicta dignissimos explicabo fugit magni tempore, temporibus ut veniam! Aut expedita ipsam maiores quod repellendus! Ab eveniet in iure labore nesciunt odio perferendis repudiandae soluta tenetur!', 'fc31335f.jpg', 'a86d05c792ba72ea459532735caca2b9', '1'),
(27, 'Lorem ipsum dolor sit amet', 'Spectacular', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias aliquam animi, culpa cum deleniti dolorem doloremque earum error eveniet, excepturi harum magni maxime modi nam nostrum odit omnis, praesentium provident quod recusandae reprehenderit saepe sequi ut voluptas. Ad aliquam, amet aspernatur delectus dolores doloribus eaque error exercitationem explicabo facere nam natus nesciunt odio quibusdam, quisquam rem repudiandae temporibus! Debitis ea enim itaque maiores mollitia necessitatibus perferendis quis rerum ullam, voluptatem! A adipisci alias dicta dignissimos explicabo fugit magni tempore, temporibus ut veniam! Aut expedita ipsam maiores quod repellendus! Ab eveniet in iure labore nesciunt odio perferendis repudiandae soluta tenetur!', '758cfc78.jpg', '758cfc7828637a5b0d6e9f4e55752076', '1'),
(29, 'dhaka', 'Mountain', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias aliquam animi, culpa cum deleniti dolorem doloremque earum error eveniet, excepturi harum magni maxime modi nam nostrum odit omnis, praesentium provident quod recusandae reprehenderit saepe sequi ut voluptas. Ad aliquam, amet aspernatur delectus dolores doloribus eaque error exercitationem explicabo facere nam natus nesciunt odio quibusdam, quisquam rem repudiandae temporibus! Debitis ea enim itaque maiores mollitia necessitatibus perferendis quis rerum ullam, voluptatem! A adipisci alias dicta dignissimos explicabo fugit magni tempore, temporibus ut veniam! Aut expedita ipsam maiores quod repellendus! Ab eveniet in iure labore nesciunt odio perferendis repudiandae soluta tenetur!', '25ae5992.jpg', '862b39280a32a7eddc680c7840cbaa8b', '1'),
(30, 'Sylet', 'Mountain', ' this is shonargao hotel\r\n                                                                                    ', '402dbba2.jpg', 'f67e6f9696c5e76421dd9bd87b296206', '1'),
(31, 'asdf', 'Spectacular', 'asdasdf\r\n            ', '3276c574.jpg', '3276c5749a33e579b502fc757efc13cc', '1'),
(32, 'asdf', 'Mountain', ' sdf                               \r\n                                                        ', 'bb6b6222.jpg', '6e7f6f4a149a808170ce4c7af9c77a05', '2'),
(33, 'Dhaka', 'Mountain', 'dhaka dhaka dhaka\r\n                            ', '07c6c59f.jpg', '07c6c59f3167d6c9bca629984db9a960', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `admin_role`
--
ALTER TABLE `admin_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
